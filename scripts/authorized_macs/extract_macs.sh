#!/bin/bash
#
# Process the FreeRADIUS logs to extract MAC addresses for users logging in to the system in format:
#   user.name__MAC-ADD-RES-SSS 
# 
# Ali Khalil
# 2014-08-13
#

FILE_TMP=/tmp/radius_mac_$(date +%Y%m%d%H%M%S).tmp

cat /var/log/freeradius/radius.log /var/log/freeradius/radius.log.1 > $FILE_TMP
cat /var/log/freeradius/radius*.gz | gunzip >> $FILE_TMP

grep 'Login OK.*port 0 cli' $FILE_TMP | sed -r 's/.*\[(.*)\/.* (.*)\)/\1__\2/g' | sort -n | uniq

rm $FILE_TMP

