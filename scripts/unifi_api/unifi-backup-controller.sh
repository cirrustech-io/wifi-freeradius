#!/bin/bash -x

## define required variables
username=admin
password=8l9641o@
baseurl=https://wifi-unifi.2connectbahrain.com:443
backup_destination=/data/backup/controller/unifi-controller-`date +%Y%m%d_%H%M%S`.unf

SCRIPT_LOCATION=`dirname $0`

## include the API library
. $SCRIPT_LOCATION/unifi_sh_api

# Log in to the UniFi Controller
unifi_login

# DO THE BACKUP
unifi_backup $backup_destination 

# Cleanly log out of the UniFi Controller
unifi_logout

